package com.twitter.timeline;

import com.github.adminfaces.template.session.AdminSession;
import org.omnifaces.util.Faces;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Specializes;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

import com.github.adminfaces.template.config.AdminConfig;
import com.twitter.utils.objConstantes;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.primefaces.json.JSONObject;

/**
 * Created by rmpestano on 12/20/14.
 *
 * This is just a login example.
 *
 * AdminSession uses isLoggedIn to determine if user must be redirect to login
 * page or not. By default AdminSession isLoggedIn always resolves to true so it
 * will not try to redirect user.
 *
 * If you already have your authorization mechanism which controls when user
 * must be redirect to initial page or logon you can skip this class.
 */
@Named
@SessionScoped
@Specializes
public class LogonMB extends AdminSession implements Serializable {

    private String currentUser;
    private String email;
    private String password;
    private boolean remember;
    private objUsuario usuario = new objUsuario();
    private FacesMessage msgP = null;
    int a;

    @Inject
    private AdminConfig adminConfig;

    public void login() throws IOException {
        currentUser = email;

        final int timeout = 400000;//1200000;//300,000 5 minutos
        String output = "";
        try {

            objConstantes cons = new objConstantes();

            Locale locale = Locale.getDefault();
            ResourceBundle datos = ResourceBundle.getBundle("LogIn", locale);

            String urlmp = datos.getString("url");
            urlmp += "login/";

            String json = "{\n"
                    + "  \"key\": \"" + cons.getKey() + "\",\n"
                    + "  \"password\": \"" + password + "\",\n"
                    + "\"user\": \"" + currentUser + "\"}";

            URL targetUrl = new URL(urlmp);

            HttpURLConnection httpConnection = (HttpURLConnection) targetUrl.openConnection();
            httpConnection.setDoOutput(true);
            httpConnection.setRequestMethod("POST");
            httpConnection.setRequestProperty("Content-Type", "application/json");

            OutputStream outputStream = httpConnection.getOutputStream();
            outputStream.write(json.getBytes("UTF-8"));
            outputStream.flush();

            if (httpConnection.getResponseCode() != 202) {
                msgP = new FacesMessage(FacesMessage.SEVERITY_INFO, "No es posible comunicarse con el web service", "");
                FacesContext.getCurrentInstance().addMessage(null, msgP);

                throw new RuntimeException("Failed : HTTP error code : "
                        + httpConnection.getResponseCode());

            } else {
                //OBTENER EL RESPONSE EL UTF-8
                BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                        (httpConnection.getInputStream()), "UTF8"));

                output = responseBuffer.readLine();

                JSONObject obj = new JSONObject(output);

                String msgPs = obj.getString("msg");
                boolean error = obj.getBoolean("error");

                if (!error) {
                    msgP = new FacesMessage(FacesMessage.SEVERITY_INFO, msgPs, "");

                    FacesContext.getCurrentInstance().addMessage(null, msgP);
                    usuario.setNombre(currentUser);
                    usuario.setPass(password);
                    usuario.setUsuario(currentUser);
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", usuario);

                    Faces.getExternalContext().getFlash().setKeepMessages(true);
                    Faces.redirect(adminConfig.getIndexPage());
                    
                    

                } else {
                    msgP = new FacesMessage(FacesMessage.SEVERITY_WARN, msgPs, "");
                    FacesContext.getCurrentInstance().addMessage(null, msgP);
                    currentUser=null;
                 //  Faces.redirect("/login.xhtml");
                                   
                }

            }

            httpConnection.disconnect();

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("ERROR --->" + ex.getMessage());
            msgP = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error: " + ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, msgP);
        }
    }

    @Override
    public boolean isLoggedIn() {

        return currentUser != null;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }
}
