package seleniumtest;
import java.io.IOException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Prueba {
    
    public static void main(String[] args) throws IOException, InterruptedException{

                    probarLoginCorrecto();
                    System.out.println("prueba 1 pasada, iniciando prueba 2");
                    probarLoginInchorrecto(); 
                    System.out.println("prueba 2 pasada, iniciando prueba 3");
                    hacerTuit();
                    
                    
                //probarLoginCorrecto();
                //probarLoginInchorrecto();
                //hacerTuit(); 
        }
        public static void probarLoginCorrecto() throws  IOException, InterruptedException{
                System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--no-sandbox");

        WebDriver driver = new ChromeDriver(chromeOptions);

        driver.get("http://35.224.213.29:5001/web-twitter/login.jsf");
        WebElement link; 
        Thread.sleep(1000);
        WebElement user= driver.findElement(By.id("j_id_f:inputEmail"));
        user.sendKeys("moino");
         System.out.println("User escrito");
        Thread.sleep(3000);
        WebElement pass= driver.findElement(By.id("j_id_f:inputPassword"));
        pass.sendKeys("admin");
         System.out.println("passwrd escrita");
        Thread.sleep(3000);
        link = driver.findElement(By.id("j_id_f:buttonSingin"));
        link.click();
         System.out.println("boton presionado");
        Thread.sleep(2000);
        if (driver.getPageSource().contains("Usuario auntenticado")) {
                System.out.println("Paso");

        } else {
                System.out.println("Fallo");
        }
        driver.quit();
        }
        public static void probarLoginInchorrecto() throws  IOException, InterruptedException{
                System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("--no-sandbox");

            WebDriver driver = new ChromeDriver(chromeOptions);

            driver.get("http://35.224.213.29:5001/web-twitter/login.jsf");
            WebElement link; 
            Thread.sleep(1000);
            WebElement user= driver.findElement(By.id("j_id_f:inputEmail"));
            user.sendKeys("moino");
             System.out.println("User escrito");
            Thread.sleep(3000);
            WebElement pass= driver.findElement(By.id("j_id_f:inputPassword"));
            pass.sendKeys("1234");
             System.out.println("passwrd escrita");
            Thread.sleep(3000);
            link = driver.findElement(By.id("j_id_f:buttonSingin"));
            link.click();
             System.out.println("boton presionado");
            Thread.sleep(2000);
            if (driver.getPageSource().contains("Contraseña incorrecta")) {
                    System.out.println("Paso, erroneo");

            } else {
                    System.out.println("Fallo");
            }
            driver.quit();
        }
        public static void hacerTuit() throws  IOException, InterruptedException{
                System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--no-sandbox");

        WebDriver driver = new ChromeDriver(chromeOptions);

        driver.get("http://35.224.213.29:5001/web-twitter/login.jsf");
        WebElement link; 
        Thread.sleep(1000);
        WebElement user= driver.findElement(By.id("j_id_f:inputEmail"));
        user.sendKeys("moino");
        System.out.println("User escrito");
        Thread.sleep(3000);
        WebElement pass= driver.findElement(By.id("j_id_f:inputPassword"));
        pass.sendKeys("admin");
        System.out.println("passwrd escrita");
        Thread.sleep(3000);
        link = driver.findElement(By.id("j_id_f:buttonSingin"));
        link.click();
        System.out.println("boton presionado");
        Thread.sleep(2000);
        if (driver.getPageSource().contains("Usuario auntenticado")) {
                System.out.println("login correcto");
        } else {
                System.out.println("Fallo login");
        }
        WebElement time = driver.findElement(By.linkText("Timeline"));
        time.click();
        Thread.sleep(2000);
        if (driver.getPageSource().contains("Write a tweet")) {
                System.out.println("timeline correcto");
        } else {
	        System.out.println("Fallo timeline");
        }
                WebElement texto = driver.findElement(By.id("form:textareaTweet"));
                texto.sendKeys("esto es un tuit");
                Thread.sleep(3000);
                WebElement link2;
                link2 = driver.findElement(By.id("form:buttonPublicar"));
                link2.click();
                Thread.sleep(3000);
                if (driver.getPageSource().contains("esto es un tuit")) {
                System.out.println("tuit correcto");
        } else {
                System.out.println("Fallo tuit");
        }
                driver.quit();
        }
}
